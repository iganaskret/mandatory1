from django.test import TestCase
from django.contrib.auth.models import User

from .models import Article

class ArticleTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Create a user
        testuser = User.objects.create_user(
                username = 'testuser', password='abc123')
        testuser.save()

        # Create a blogpost
        test_article = Article.objects.create(author=testuser, title='Title', body='Content')
        test_article.save()

    def test_article_content(self):
        article = Article.objects.get(id=1)
        expected_author = f'{article.author}'
        expected_title = f'{article.title}'
        expected_body = f'{article.body}'
        self.assertEqual(expected_author, 'testuser')
        self.assertEqual(expected_title, 'Title')
        self.assertEqual(expected_body, 'Content')


# Create your tests here.
