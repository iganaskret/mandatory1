FROM python:3.8-slim-buster
RUN pip install --upgrade pip
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
ENTRYPOINT ["sh", "./entrypoint.sh"]

