#!/bin/sh

echo "starting entrypoint script"
python manage.py makemigrations
python manage.py migrate
python manage.py test
python manage.py runserver 0.0.0.0:8000
#python manage.py runserver 172.104.128.162:8000
